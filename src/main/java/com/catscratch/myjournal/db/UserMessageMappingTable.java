package com.catscratch.myjournal.db;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.catscratch.myjournal.model.BasicJournalEntryId;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

@Component
public class UserMessageMappingTable {

	@Value("${mongo.host}")
	private String mongoHost;
	
	@Value("${mongo.port}")
	private String mongoPort;
	
	@Value("${mongo.table.userMessageMapping}")
	private String messageMappingTableName;
	
	@Value("${mongo.username}")
	private String messageMappingTableUsername;
	
	@Value("${mongo.password}")
	private String messageMappingTablePassword;
	
	private MongoCollection<Document> messageMappingTable;
	
	@PostConstruct
	public void init() {
		String url = "mongodb://" + this.messageMappingTableUsername + ":" + this.messageMappingTablePassword + "@" + this.mongoHost + ":" + this.mongoPort + "/mysecretjournal";
		
		MongoClientURI uri = new MongoClientURI(url);
		MongoClient client = new MongoClient(uri);
		MongoDatabase db = client.getDatabase(uri.getDatabase());
		
		this.messageMappingTable = db.getCollection(this.messageMappingTableName);
	}
	
	public boolean addMessageToUser(String userId, String messageId) {
		// TODO Auto-generated method stub
		BasicDBObject searchQuery = new BasicDBObject("userId", userId);
		
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/YYYY hh:mm a");
		String currentTime = now.format(formatter);
		
		BasicDBObject entry = new BasicDBObject();
		entry.put("name", messageId);
		entry.put("created", currentTime);
		entry.put("modified", currentTime);
		BasicDBObject messageAdding = new BasicDBObject("messages", entry);
		
		BasicDBObject appendQuery = new BasicDBObject("$addToSet", messageAdding);
		
		long numberUpdated = this.messageMappingTable.updateOne(searchQuery, appendQuery).getModifiedCount();
		
		return (numberUpdated == 1);
	}
	
	public boolean createUserMessageMappingNewUser(String userId) {
		Document userMessageMappingRow = new Document();
		userMessageMappingRow.put("userId", userId);
		userMessageMappingRow.put("messages", new ArrayList<>());
		
		try {
			this.messageMappingTable.insertOne(userMessageMappingRow);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public boolean updateMessageTimestamp(String userId, String messageId) {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm a");
		String currentTime = now.format(formatter);
		BasicDBObject updateObject = new BasicDBObject("messages.$.modified", currentTime);
		
		BasicDBObject updateQuery = new BasicDBObject("$set", updateObject);
		
		BasicDBObject searchObject = new BasicDBObject("userId", userId);
		searchObject.put("messages.name", messageId);
		
		return this.messageMappingTable.updateOne(searchObject, updateQuery).getModifiedCount() > 0;
	}
	
	public List<BasicJournalEntryId> getMessageDataAssociatedWithUser(String userId) {
		BasicDBObject searchCriteria = new BasicDBObject("userId", userId);
		
		MongoCursor<Document> results = this.messageMappingTable.find(searchCriteria).iterator();
		List<Document> messages = (List<Document>) results.next().get("messages");
		
		List<BasicJournalEntryId> ids = new ArrayList<>();
		for (Document messageData : messages) {
			BasicJournalEntryId id = new BasicJournalEntryId();
			id.setName(messageData.getString("name"));
			id.setCreated(messageData.getString("created"));
			id.setLastModified(messageData.getString("modified"));
			
			ids.add(id);
		}
		
		return ids;
	}

	
	
}
