package com.catscratch.myjournal.db;

import javax.annotation.PostConstruct;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.catscratch.myjournal.model.PasswordDetails;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

@Component
public class LoginTable {

	@Value("${mongo.host}")
	private String mongoHost;
	
	@Value("${mongo.port}")
	private String mongoPort;
	
	@Value("${mongo.table.login}")
	private String loginTableName;
	
	@Value("${mongo.username}")
	private String loginTableUsername;
	
	@Value("${mongo.password}")
	private String loginTablePassword;
	
	private MongoCollection<Document> loginTable;
	
	@PostConstruct
	private void init() {
		String url = "mongodb://" + this.loginTableUsername + ":" + this.loginTablePassword + "@" + this.mongoHost + ":" + this.mongoPort + "/mysecretjournal";
		
		MongoClientURI uri = new MongoClientURI(url);
		MongoClient client = new MongoClient(uri);
		MongoDatabase db = client.getDatabase(uri.getDatabase());
		
		this.loginTable = db.getCollection(this.loginTableName);
	}
	
	public PasswordDetails getUserPasswordDetails(String user) {
		BasicDBObject userFilter = new BasicDBObject("username", user);
		MongoCursor<Document> users = this.loginTable.find(userFilter).iterator();
		
		if (!users.hasNext()) {
			return null;
		}
		
		Document userObject = users.next();
		
		String passwordHash = (String) userObject.get("hash");
		String salt = (String) userObject.get("salt");
		
		if (passwordHash != null 
				&& salt != null) {
			return new PasswordDetails(passwordHash, salt);
		}
		return null;
	}

	public boolean createUser(String userId, String hash, String salt) {
		Document userData = new Document();
		userData.put("username", userId);
		userData.put("hash", hash);
		userData.put("salt", salt);
		
		try {
			this.loginTable.insertOne(userData);	
		} catch (Exception e) {
			return false;
		}
		return true;
		
	}
	
	public boolean doesUserExist(String userId) {
		BasicDBObject idFilter = new BasicDBObject("username", userId);
		MongoCursor<Document> iterator = this.loginTable.find(idFilter).iterator();
		
		return iterator.hasNext();
	}
	
}
