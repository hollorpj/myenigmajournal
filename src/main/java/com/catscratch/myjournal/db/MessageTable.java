package com.catscratch.myjournal.db;

import java.util.List;

import javax.annotation.PostConstruct;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.catscratch.myjournal.exceptions.DaoException;
import com.catscratch.myjournal.model.message.MessageRetrievalContainer;
import com.catscratch.myjournal.model.message.MessageSubmission;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

@Component
public class MessageTable {

	@Value("${mongo.host}")
	private String mongoHost;
	
	@Value("${mongo.port}")
	private String mongoPort;
	
	@Value("${mongo.table.messages}")
	private String messageTableName;
	
	@Value("${mongo.username}")
	private String messageTableUsername;
	
	@Value("${mongo.password}")
	private String messageTablePassword;
	
	private MongoCollection<Document> messageTable;
	
	@PostConstruct
	public void init() {
		String url = "mongodb://" + this.messageTableUsername + ":" + this.messageTablePassword + "@" + this.mongoHost + ":" + this.mongoPort + "/mysecretjournal";
		
		MongoClientURI uri = new MongoClientURI(url);
		MongoClient client = new MongoClient(uri);
		MongoDatabase db = client.getDatabase(uri.getDatabase());
		
		this.messageTable = db.getCollection(this.messageTableName);
	}
	
	public boolean addMessage(String user, String messageId, List<String> messageContent) {
		// todo : map user to their message IDs
		
		Document messageTableEntry = new Document();
		messageTableEntry.put("userId",  user);
		messageTableEntry.put("messageId",  messageId);
		messageTableEntry.put("messageContent",  messageContent);
		
		try {
			this.messageTable.insertOne(messageTableEntry);	
		} catch (Exception e) {
			return false;
		}
		
		return true;
	}
	
	public boolean updateMessage(String user, String messageId, List<String> messageContent) {
		BasicDBObject updateQuery = new BasicDBObject();
		BasicDBObject updatedMessage = new BasicDBObject("messageContent", messageContent);
		updateQuery.append("$set", updatedMessage);
		
		Document search = new Document();
		search.put("userId", user);
		search.put("messageId", messageId);
		
		return this.messageTable.updateOne(search, updateQuery).getModifiedCount() > 0;
	}
	
	public MessageRetrievalContainer retrieveMessage(String userId, String messageId) throws DaoException {
		BasicDBObject messageFilter = new BasicDBObject();
		messageFilter.put("userId", userId);
		messageFilter.put("messageId", messageId);
		
		MongoCursor<Document> iterator = this.messageTable.find(messageFilter).iterator();
		if (!iterator.hasNext()) {
			throw new DaoException("Cannot find message " + messageId + " for " + userId);
		}
		
		Document messageDocument = iterator.next();
		List<String> message = (List<String>) messageDocument.get("messageContent");
		MessageRetrievalContainer messageObject = new MessageRetrievalContainer(messageId, message);
		
		return messageObject;
	}
	
	public boolean doesMessageExist(String userId, String messageId) {
		BasicDBObject messageFilter = new BasicDBObject();
		messageFilter.put("userId", userId);
		messageFilter.put("messageId", messageId);
		
		return this.messageTable.find(messageFilter).iterator().hasNext();
	}
	
}
