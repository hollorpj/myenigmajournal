package com.catscratch.myjournal.security;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.catscratch.myjournal.model.UserSessionIdentifier;

@Component
public class CookieManagement {

	@Value("${security.cookie.ttl}")
	private long timeToLive;
	
	private Timer cookieCustodian;
	
	private final Map<String, UserSessionIdentifier> cookieJar;
	private final Map<Long, String> cookieExpiration;
	
	public CookieManagement() {
		this.cookieJar = new HashMap<String, UserSessionIdentifier>();
		this.cookieExpiration = new TreeMap<Long, String>();
		TimerTask task = new TimerTask() {

			@Override
			public void run() {
				cleanupExpiredCookies();
			}
			
		};
		this.cookieCustodian = new Timer();
		cookieCustodian.schedule(task, 0l, 60 * 1000);
	}
	
	public String associateCookieWithUser(String user) {
		// current date plus time to live formatted in EEE, dd MMM yyyy hh:mm:ss
		long expirationDate = System.currentTimeMillis() + this.timeToLive;
		
		String uuid = UUID.randomUUID().toString();
		String cookieKey = "cookie=" + user + "^" + uuid;
		String userCookie = cookieKey + ";max-age=" + this.timeToLive / 1000;
		
		// duplicate check - we need to remove the old cookie from the expiration list if there is a match
		if (this.cookieJar.containsKey(user)) {
			long oldExpiration = this.cookieJar.get(user).getExpirationDate();
			this.cookieExpiration.remove(oldExpiration);
		}
		
		// We dont want a collision between two cookies with the same xpiration date - If a collision is imminent, just increment the new cookie expiration by 1
		while (this.cookieExpiration.containsKey(expirationDate)) {
			expirationDate += 1;
		}
		
		// Add cookie to jar for authentication purposes, add to cookieExpiration for cleanup purposes (We don't want to have it be that a user who logs in 2018 still has a stored cookie in 2038!)
		this.cookieJar.put(user, new UserSessionIdentifier(uuid, expirationDate));
		this.cookieExpiration.put(expirationDate, user);
		
		return userCookie;
	}
	
	public boolean isCookieValid(String cookie) {
		String user = cookie.split("\\^")[0];
		String uuid = cookie.split("\\^")[1];
		if (!cookieJar.containsKey(user)) {
			return false;
		}
		
		if (!cookieJar.get(user).getUuid().equals(uuid)) {
			return false;
		}
		
		if (cookieJar.get(user).getExpirationDate() < System.currentTimeMillis()) {
			cookieExpiration.remove(cookieJar.get(user).getExpirationDate());
			cookieJar.remove(user);
		    return false;	
		}
		
		return true;
	}
	
	private void cleanupExpiredCookies() {
		if (this.cookieExpiration.size() == 0) {
			return;
		}
		
		// is a treeset, so can guarantee natural order
		List<Long> expirations = new ArrayList<Long>(this.cookieExpiration.keySet());
		long currentTime = System.currentTimeMillis();
		
		int listSize = expirations.size();
		int index = expirations.size() / 2;
		long lastLarger = listSize - 1;
		while (true) {
			if (expirations.get(index) >= currentTime) {
				for (int i = index; i < listSize; i++) {
					this.cookieExpiration.remove(expirations.get(index).longValue());
					expirations.remove(expirations.get(index));
					i--;
					listSize--;
				}
				
				if (index == 0 
						|| expirations.get(index - 1) < currentTime) {
					break;
				}
				
				lastLarger = index;
				index = (int) (lastLarger / 2);
				
				continue;
			}
			
			if (expirations.get(index) < currentTime) {
				index = (int) ((index + lastLarger) / 2);
				continue;
			}
			
		}
	}
	
//	public static Set<Long> c(Set<Long> ls, long currentTime) {
//		// is a treeset, so can guarantee natural order
//		List<Long> expirations = new ArrayList<Long>(ls);
//		
//		int listSize = expirations.size();
//		int index = expirations.size() / 2;
//		long lastLarger = listSize - 1;
//		int counter = 0;
//		while (true) {
//			counter++;
//			
//			if (expirations.get(index) >= currentTime) {
//				for (int i = index; i < listSize; i++) {
//					expirations.remove(index);
//					i--;
//					listSize--;
//				}
//				
//				if (expirations.get(index - 1) < currentTime) {
//					break;
//				}
//				
//				lastLarger = index;
//				index = (int) (lastLarger / 2);
//				
//				continue;
//			}
//			
//			if (expirations.get(index) < currentTime) {
//				index = (int) ((index + lastLarger) / 2);
//				continue;
//			}
//			
//		}
//		System.out.println(counter);
//		
//		return new HashSet<Long>(expirations);
//	}
//	
//	public static void main(String[] args) {
//		Set<Long> ls = new TreeSet<Long>();
//		for (int i = 0; i < 1000000; i++) {
//			ls.add((long) i);
//		}
//		
//		long start = System.currentTimeMillis();
//		c(ls, 4096);
//		System.out.println(System.currentTimeMillis() - start);
//		
//		start = System.currentTimeMillis();
//		List<Long> l = new ArrayList<Long>(ls);
//		for (int i = 0; i < l.size(); i++) {
//			if (l.get(i) >= 4096) {
//				l.remove(i);
//				i--;
//			} 
//		}
//		System.out.println(System.currentTimeMillis() - start);
//		
//	}
	
}
