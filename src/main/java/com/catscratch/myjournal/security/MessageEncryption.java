
package com.catscratch.myjournal.security;

import java.math.BigInteger;


public class MessageEncryption {

	public BigInteger[] encryptMessage(String message, BigInteger p, BigInteger q, int e) {
		BigInteger n = p.multiply(q); 
		
		char[] rawMessageChs = message.toCharArray();
		BigInteger[] encrypted = new BigInteger[rawMessageChs.length];
		for (int i = 0; i < rawMessageChs.length; i++) {
			encrypted[i] = encrypt(rawMessageChs[i], e, n);
		}
		
		return encrypted;
	}
	
	public String decryptMessage(BigInteger[] message, BigInteger d, BigInteger n) {
		StringBuilder decrypted = new StringBuilder();
		for (BigInteger hiddenCh : message) {
			decrypted.append(decrypt(hiddenCh, d, n));
		}
		return decrypted.toString();
	}
	
	public static void main(String[] args) {
		MessageEncryption enc = new MessageEncryption();
		
		String message = "hello rsa";
		BigInteger p = new BigInteger("43196704335708768786328328239233503042274638239456997145577617365310310397849097151850385760746291543328452513030611352880311371862027277620950684659471947677687718251558059937618554947885834745477183684556630424011432051306540310734553023453714483447143958037192973999450308230073414750429534685750382326176430758544678339828367427203166273357661947770494196220676738189083214447060047851630740734336801295915254012816024310024097408079628974991852074358773401260787390526756967782964071918218705093259405260477");
		BigInteger q = new BigInteger("75082175448174644536517509799533495175597286840175915419398294801838877277270652082401363905959459014675006132501840704245197595505256334610382492233939330467756462478353258409307345905907239078335011495241454203921948814715354267447473088472233761893952906785263893540729403572383826959721911647176741120389673891867778854445804999598102714626812521412411005900294422380771435421480779857655579714689627400780850823203693805343316705930262086441453202126638084212638977104015306688924675481846563761073472628767");
		BigInteger totient = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
		BigInteger d = new BigInteger("7").modPow(new BigInteger("-1"), totient);
		BigInteger n = p.multiply(q);
		
		BigInteger[] hidden = enc.encryptMessage(message, p, q, 7);
		String mu = enc.decryptMessage(hidden, d, n);
		System.out.println(mu);
	}
	
	private BigInteger encrypt(int toEncrypt, int e, BigInteger n) {
		BigInteger encrypted = new BigInteger(String.valueOf(toEncrypt)).pow(e).mod(n);
		return encrypted;
	}
	
	private char decrypt(BigInteger toDecrypt, BigInteger d, BigInteger n) {
		return (char) toDecrypt.modPow(d, n).intValue();
	}

	
}
