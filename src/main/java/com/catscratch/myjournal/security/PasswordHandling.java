package com.catscratch.myjournal.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.stereotype.Component;


@Component
public class PasswordHandling {

	
	public String determineSalt(String user, long timeUserSignedUp, long similarityToMyFavorite) {
		int seedOne = ((int) (Math.random() * 9581775));
		while (seedOne == 0) {
			seedOne = ((int) (Math.random() * 9581775));
		}
		int seedTwo = seedOne * (int) (Math.random() * 127283);
		while (seedTwo == 0) {
			seedTwo = seedOne * (int) (Math.random() * 1274283);
		}
		int seedThree = seedTwo * (int) (Math.random() * 1609221);
		while (seedThree == 0) {
			seedThree = seedTwo * (int) (Math.random() * 1609221);
		}
		
		String salt = String.valueOf(seedThree);
		
		return salt;
	}
	
	public String hashUsingSha2(String str) {
		try {
			MessageDigest digestor = MessageDigest.getInstance("SHA-256");
		
			byte[] hashed = digestor.digest(str.getBytes());
			
			return new String(hashed);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
//	public static void main(String[] args) {
//		PasswordHandling pw = new PasswordHandling();
//		
//		String user1 = "holleyRun";
//		String user2 = "hammerholder";
//		
//		String password = "AhRats";
//		String salt = pw.determineSalt("holleyRun", 162432000, 81649);
//		String salt2 = pw.determineSalt("hammerholder", 210432000, 91729);
//		
//		String hsh1 = pw.hashUsingSha2(password + salt);
//		String hsh2 = pw.hashUsingSha2(password + salt2);
//		
//		String rsh1 = pw.hashUsingSha2(password + salt);
//		String rsh2 = pw.hashUsingSha2(password + salt2);
//		
//		System.out.println(hsh1.equals(rsh1));
//		System.out.println(hsh2.equals(rsh2));
//	}
	
	
}
