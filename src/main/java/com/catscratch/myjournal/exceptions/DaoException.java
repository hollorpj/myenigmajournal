package com.catscratch.myjournal.exceptions;

public class DaoException extends Exception {

	public DaoException(String message) {
		super(message);
	}
	
}
