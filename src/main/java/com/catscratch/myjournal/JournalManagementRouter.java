package com.catscratch.myjournal;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.catscratch.myjournal.db.MessageTable;
import com.catscratch.myjournal.db.UserMessageMappingTable;
import com.catscratch.myjournal.exceptions.DaoException;
import com.catscratch.myjournal.model.BasicJournalEntryId;
import com.catscratch.myjournal.model.CookieHolder;
import com.catscratch.myjournal.model.EntryRequest;
import com.catscratch.myjournal.model.EntrySave;
import com.catscratch.myjournal.model.message.MessageRetrievalContainer;
import com.catscratch.myjournal.operations.AccountManagement;

@RestController
public class JournalManagementRouter {

	@Autowired
	private AccountManagement accountManager;
	
	@Autowired
	private UserMessageMappingTable userMessageMappingTable;
	
	@Autowired
	private MessageTable messageTable;
	
	@RequestMapping(value="/dummy", method=RequestMethod.GET)
	public String dummy() {
		return "Hi";
	}
	
	@CrossOrigin("http://localhost:4200")
	@RequestMapping(value="/fetchNames", method=RequestMethod.POST)
	public ResponseEntity<List<BasicJournalEntryId>> fetchNames(@RequestBody CookieHolder entity) {
		if (entity == null || entity.getCookie() == null) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		
		String userName = entity.getCookie().split("\\^")[0];
		String cookie = entity.getCookie();
		
		if (this.accountManager.isCookieAlive(cookie)) {
			List<BasicJournalEntryId> journalMetadata = this.userMessageMappingTable.getMessageDataAssociatedWithUser(userName);
			
			return new ResponseEntity<>(journalMetadata, HttpStatus.OK);
		}
		
		return new ResponseEntity<>(HttpStatus.FORBIDDEN);
	}
	
	@CrossOrigin("http://localhost:4200")
	@RequestMapping(value="/fetchEntry", method=RequestMethod.POST)
	public ResponseEntity<String> fetchEntry(@RequestBody EntryRequest entity) throws DaoException {
		String cookie = entity.getCookie();
		String username = cookie.split("\\^")[0];
		
		
		if (this.accountManager.isCookieAlive(cookie)) {
			MessageRetrievalContainer messageContainer = this.messageTable.retrieveMessage(username, entity.getId());
			List<String> messageContent = messageContainer.getMessageContent();
			
			return new ResponseEntity(messageContent, HttpStatus.OK);
		}
		
		return new ResponseEntity<>(HttpStatus.FORBIDDEN);
	}
	
	@CrossOrigin("http://localhost:4200")
	@RequestMapping(value="/saveEntry", method=RequestMethod.POST)
	public ResponseEntity<String> saveEntry(@RequestBody EntrySave entity) {
		String cookie = entity.getCookie();
		String username = cookie.split("\\^")[0];
		
		if (this.accountManager.isCookieAlive(cookie)) {
			String messageId = entity.getEntryId();
			if (this.messageTable.doesMessageExist(username, messageId)) {
				this.messageTable.updateMessage(username, messageId, entity.getContent());
				this.userMessageMappingTable.updateMessageTimestamp(username, messageId);
			} else {
				this.userMessageMappingTable.addMessageToUser(username, messageId);
				this.messageTable.addMessage(username, messageId, entity.getContent());
			}
			
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
		return new ResponseEntity<>(HttpStatus.FORBIDDEN);
	}
	
}
