package com.catscratch.myjournal.operations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.catscratch.myjournal.db.LoginTable;
import com.catscratch.myjournal.db.UserMessageMappingTable;
import com.catscratch.myjournal.email.EmailSender;
import com.catscratch.myjournal.model.BrowserCookie;
import com.catscratch.myjournal.model.LoginObject;
import com.catscratch.myjournal.model.PasswordDetails;
import com.catscratch.myjournal.model.RSAParameters;
import com.catscratch.myjournal.security.CookieManagement;
import com.catscratch.myjournal.security.PasswordHandling;

@Component
public class AccountManagement {

	@Autowired
	private PasswordHandling passwordHandler;
	
	@Autowired
	private CookieManagement cookieManager;
	
	@Autowired
	private LoginTable loginDao;
	
	@Autowired
	private UserMessageMappingTable userMessageMappingDao;
//	
//	@Autowired
//	private KeyGeneration keyGenerator;
	
	@Autowired
	private EmailSender emailSender;
	
	public boolean createUser(LoginObject creationObject) {
		long timeSignedUp = System.currentTimeMillis();
		String userId = creationObject.getUserId();
		String salt = this.passwordHandler.determineSalt(creationObject.getUserId(), timeSignedUp, 22475);
		String hash = this.passwordHandler.hashUsingSha2(creationObject.getPassword() + salt);
		
		boolean status = this.loginDao.createUser(userId, hash, salt);
		if (!status) {
			return false;
		}
		
		return this.userMessageMappingDao.createUserMessageMappingNewUser(userId);
	}

	public String login(LoginObject login) {
		String userId = login.getUserId();
		String password = login.getPassword();
		
		PasswordDetails passwordDetails = loginDao.getUserPasswordDetails(userId);
		if (passwordDetails == null) {
			return null;
		}
		
		password += passwordDetails.getSalt();
		String hashedInput = this.passwordHandler.hashUsingSha2(password);
		
		if (hashedInput.equals(passwordDetails.getPasswordHash())) {
			return cookieManager.associateCookieWithUser(userId);
		}
		
		return null;
	}

	public boolean isCookieAlive(String cookie) {
		return this.cookieManager.isCookieValid(cookie);
	}

	public boolean doesUserAlreadyExist(String userId) {
		return loginDao.doesUserExist(userId);
	}

//	public void sendPrivateKeyToEmail(String email) {
//		RSAParameters keyPair = this.keyGenerator.generateRsaParameters("512");
//			
//		String keyOne = keyPair.getP();
//		String keyTwo = keyPair.getQ();
//		String e = keyPair.getE();
//		String d = keyPair.getD();
//		
//		String messageContent = keyOne + "\n" + keyTwo + "\n" + e + "\n" + d;
//		
//		this.emailSender.sendEmailWithAttachment(email, "Your Secret Journal Key", "hi", messageContent, "JournalKey.txt");
//	}
	
	
}
