package com.catscratch.myjournal;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.Cookie;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.catscratch.myjournal.db.LoginTable;
import com.catscratch.myjournal.model.BrowserCookie;
import com.catscratch.myjournal.model.LoginObject;
import com.catscratch.myjournal.model.PasswordDetails;
import com.catscratch.myjournal.operations.AccountManagement;
import com.catscratch.myjournal.security.CookieManagement;
import com.catscratch.myjournal.security.PasswordHandling;

@RestController
public class AccountManagementRouter {
	
	@Autowired
	private AccountManagement accountManager;

	@CrossOrigin("http://localhost:4200")
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public ResponseEntity<String> login(@RequestBody LoginObject login) {
		String cookie = accountManager.login(login);
		if (cookie == null) {
			ResponseEntity<String> response = new ResponseEntity<String>(HttpStatus.FORBIDDEN);
			return response;
		}
		
		ResponseEntity<String> response = new ResponseEntity<String>(cookie, null, HttpStatus.OK);
		
		return response;
	}
	
	@CrossOrigin("http://localhost:4200")
	@RequestMapping(value="/createUser", method=RequestMethod.POST)
	public ResponseEntity<Void> createUser(@RequestBody LoginObject creationObject) {
		if (accountManager.doesUserAlreadyExist(creationObject.getUserId())) {
			return new ResponseEntity<Void>(null, null, HttpStatus.CONFLICT);
		}

		// TODO: If private key step fails, then we need to delete the user
		accountManager.createUser(creationObject);
//		accountManager.sendPrivateKeyToEmail(creationObject.getEmail());
		
		return new ResponseEntity<Void>(null, null, HttpStatus.OK);
	}
	
	public static void main(String[] args) {
		Set<Long> treeSet = new TreeSet<Long>();
		treeSet.add(1l);
		treeSet.add(2l);
		
		System.out.println(treeSet);
	}
}
