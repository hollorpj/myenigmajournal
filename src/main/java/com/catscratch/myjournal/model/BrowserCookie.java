package com.catscratch.myjournal.model;

public class BrowserCookie {
	
	private String userId;
	private String cookieId;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getCookieId() {
		return cookieId;
	}
	public void setCookieId(String cookieId) {
		this.cookieId = cookieId;
	}
	
	

}
