package com.catscratch.myjournal.model;

public class UserSessionIdentifier {

	private final String uuid;
	private final long expirationDate;
	
	public UserSessionIdentifier(String uuid, long expirationDate) {
		this.uuid = uuid;
		this.expirationDate = expirationDate;
	}

	public String getUuid() {
		return uuid;
	}

	public long getExpirationDate() {
		return expirationDate;
	}
	
	
	
}
