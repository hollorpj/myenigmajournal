package com.catscratch.myjournal.model;

public class RSAParameters {
	
	private final String p;
	private final String q;
	
	private final String e;
	private final String d;
	
	public RSAParameters(String p, String q, String e, String d) {
		this.p = p;
		this.q = q;
		this.e = e;
		this.d = d;
	}

	public String getP() {
		return p;
	}

	public String getQ() {
		return q;
	}

	public String getD() {
		return d;
	}
	
	public String getE() {
		return e;
	}
	

}
