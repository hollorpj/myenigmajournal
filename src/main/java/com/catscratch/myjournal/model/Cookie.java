package com.catscratch.myjournal.model;

public class Cookie {

	private final String cookieId;
	private final String uuid;
	
	public Cookie(String cookieId) {
		this.cookieId = cookieId;
		this.uuid = null;
	}
	
	public Cookie(String cookieId, String uuid) {
		this.cookieId = cookieId;
		this.uuid = uuid;
	}
	
	public String getCookieId() {
		return this.cookieId;
	}
	
	public String getUuid() {
		return this.uuid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cookieId == null) ? 0 : cookieId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cookie other = (Cookie) obj;
		if (cookieId == null) {
			if (other.cookieId != null)
				return false;
		} else if (!cookieId.equals(other.cookieId))
			return false;
		return true;
	}
	
	
	
}
