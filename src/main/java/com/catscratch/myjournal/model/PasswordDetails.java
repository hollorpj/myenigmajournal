package com.catscratch.myjournal.model;

public class PasswordDetails  {

	private String passwordHash;
	private String salt;
	
	public PasswordDetails(String passwordHash, String salt) {
		this.passwordHash = passwordHash;
		this.salt = salt;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}
	
	
}
