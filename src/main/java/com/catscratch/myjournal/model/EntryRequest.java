package com.catscratch.myjournal.model;

public class EntryRequest {
	
	private String cookie;
	private String id;
	public String getCookie() {
		return cookie;
	}
	public void setCookie(String cookie) {
		this.cookie = cookie;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	
}
