package com.catscratch.myjournal.model.message;

public class MessageSubmission {
	
	private String cookie;
	private String message;

	public String getCookie() {
		return cookie;
	}
	public void setCookie(String cookie) {
		this.cookie = cookie;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	

}
