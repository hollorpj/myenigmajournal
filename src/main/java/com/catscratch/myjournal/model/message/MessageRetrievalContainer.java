package com.catscratch.myjournal.model.message;

import java.util.List;

public class MessageRetrievalContainer {
	
	private String messageId;
	private List<String> messageContent;
	
	public MessageRetrievalContainer(String messageId, List<String> messageContent) {
		this.messageId = messageId;
		this.messageContent = messageContent;
	}
	
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public List<String> getMessageContent() {
		return messageContent;
	}
	public void setMessageContent(List<String> messageContent) {
		this.messageContent = messageContent;
	}
	
	

}
