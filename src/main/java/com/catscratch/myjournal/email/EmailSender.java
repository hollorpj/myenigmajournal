package com.catscratch.myjournal.email;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.annotation.PostConstruct;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EmailSender {

	@Value("${email.username}")
	private String defaultUsername;
	
	@Value("${email.password}")
	private String defaultPassword;
	
	private Properties props;
	
	private Session emailSession;
	
	
	@PostConstruct()
	public void postConstruct() {
		props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		
		this.emailSession = Session.getInstance(props, new javax.mail.Authenticator() {
		    protected PasswordAuthentication getPasswordAuthentication() {
		        return new PasswordAuthentication(defaultUsername, defaultPassword);
		    }
		});
	}
	
	public void sendEmailWithAttachment(String recepient, String title, String textContent, String attachmentContent, String attachmentName) {
		// generate attachment
		String filename = "src/main/resources/temp/" + attachmentName;
		File file = new File(filename);
		
		try {
			PrintWriter pw = new PrintWriter(new FileWriter(file));
			pw.println(attachmentContent);
			pw.close();
			
			// Build base message
			Message message = new MimeMessage(this.emailSession);
			message.setFrom(new InternetAddress(this.defaultUsername));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recepient));
			message.setSubject(title);
			message.setText(textContent);
			
			// Create multipart - file and text
			Multipart multipart = new MimeMultipart();
			
			MimeBodyPart filePiece = new MimeBodyPart();
			FileDataSource fileSource = new FileDataSource(file);
			filePiece.setDataHandler(new DataHandler(fileSource));
			filePiece.setFileName(attachmentName);

			MimeBodyPart textPiece = new MimeBodyPart();
			textPiece.setText(textContent);
			
			multipart.addBodyPart(filePiece);
			multipart.addBodyPart(textPiece);
			
			// put multipart into message and send
			message.setContent(multipart);
			
			Transport.send(message);
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			file.delete();
		}
	}
	
}
